<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionPulsaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_pulsa', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('denom_id')->nullable(false);
            $table->bigInteger('user_id')->nullable(false);
            $table->bigInteger('amount')->nullable(false);
            $table->string('customer_number')->nullable(false);
            $table->string('status')->nullable(false);
            $table->integer('balance')->nullable(false);
            $table->integer('admin_fee')->nullable(false);
            $table->text('client_response')->nullable(true);
            $table->char('ref_id')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_pulsa');
    }
}
