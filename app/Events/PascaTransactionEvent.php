<?php

namespace App\Events;

class PascaTransactionEvent extends Event
{
    public $pascaTransaction;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($denomId, $userId, $amount, $customerNumber, $customerName, $status, $clientResponse, $type, $tarif, $period, $voltage, $refId,$balance,$admin_fee)
    {
        $this->pascaTransaction['denom_id'] = $denomId;
        $this->pascaTransaction['user_id'] = $userId;
        $this->pascaTransaction['amount'] = $amount;
        $this->pascaTransaction['customer_number'] = $customerNumber;
        $this->pascaTransaction['customer_name'] = $customerName;
        $this->pascaTransaction['status'] = $status;
        $this->pascaTransaction['client_response'] = $clientResponse;
        $this->pascaTransaction['type'] = $type;
        $this->pascaTransaction['tarif'] = $tarif;
        $this->pascaTransaction['period'] = $period;
        $this->pascaTransaction['voltage'] = $voltage;
        $this->pascaTransaction['ref_id'] = $refId;
        $this->pascaTransaction['balance'] = $balance;
        $this->pascaTransaction['admin_fee'] = $admin_fee;
    }
}
