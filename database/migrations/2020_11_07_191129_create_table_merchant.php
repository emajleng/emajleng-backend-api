<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMerchant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable(false);
            $table->string('name')->nullable(false);
            $table->string('logo');
            $table->text('address')->nullable(false);
            $table->string('latitude')->nullable(false);
            $table->string('longitude')->nullable(false);
            $table->bigInteger('village_id')->nullable(false);
            $table->bigInteger('district_id')->nullable(false);
            $table->bigInteger('city_id')->nullable(false);
            $table->bigInteger('province_id')->nullable(false);
            $table->boolean('status');
            $table->string('siup_image');
            $table->boolean('siup_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_merchant');
    }
}
