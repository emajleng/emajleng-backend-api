<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chats';

    protected $fillable = [
        'user_id',
        'merchant_id',
        'message',
        'recipient'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function merchant(){
        return $this->belongsTo('App\Models\Merchant','merchant_id','id');
    }
}
