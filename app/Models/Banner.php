<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';

    protected $fillable = [
        'image',
        'description',
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];
}
