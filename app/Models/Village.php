<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $fillable = [
        'id',
        'village_id',
        'name',
    ];

    public function merchant()
    {
        return $this->hasMany('App\Models\Merchant', 'village_id');
    }

    public function districts(){
        return $this->belongsTo('App\Models\Districts','district_id','id');
    }

    public function user()
    {
        return $this->hasMany('App\Models\User', 'village_id');
    }
}
