<?php

namespace App\Events;

class NotificationEvent extends Event
{
    public $data;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, $user_id)
    {
        $this->data['data'] = $data;
        $this->data['user_id'] = $user_id;
    }
}
