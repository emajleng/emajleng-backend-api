<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Merchant;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function getAllProduct(Request $request){
        $query =  Product::query()
            ->where('name','LIKE','%'.$request->name.'%')
            ->where('stock','>',0)
            ->where('status',true)
            ->orderBy('created_at','DESC');
        $totalPage = ceil($query
                ->get()
                ->count() / 10);
        $products = $query->skip($request['page']*10)
            ->take(10)->get();
        $data=[];
        foreach ($products as $product){
            if($product->merchant->status==true){
                $response=[
                    'id' => $product->id,
                    'productCategory' => $product->productCategory,
                    'name' => $product->name,
                    'merchant' => [
                        'id' => $product->merchant->id,
                        'phone' => $product->merchant->user->phone,
                        'name' => $product->merchant->name,
                        'logo' => url($product->merchant->logo),
                        'address' => $product->merchant->address,
                        'province' => $product->merchant->province,
                        'city' => $product->merchant->city,
                        'district' => $product->merchant->districts,
                        'village' => $product->merchant->village
                    ],
                    'price' => $product->price,
                    'stock' => $product->stock,
                    'weight' => $product->weight,
                    'description' => $product->description,
                    'photo' => url($product->photo),
                    'created_at' => $product->created_at,
                    'updated_at' => $product->updated_at,
                ];
                array_push($data,$response);
            }
        }
        return ResponseHelper::paging($data,$request->page,$totalPage);
    }

    public function getProductByCategory(Request $request,$id){
        $query =  Product::query()
            ->where('product_category_id',$id)
            ->where('name','LIKE','%'.$request->name.'%')
            ->where('stock','>',0)
            ->where('status',true)
            ->orderBy('created_at','DESC');
        $totalPage = ceil($query
                ->get()
                ->count() / 10);
        $products = $query->skip($request['page']*10)
            ->take(10)->get();
        $data=[];
        foreach ($products as $product){
            if($product->merchant->status==true) {
                $response = [
                    'id' => $product->id,
                    'productCategory' => $product->productCategory,
                    'name' => $product->name,
                    'merchant' => [
                        'id' => $product->merchant->id,
                        'phone' => $product->merchant->user->phone,
                        'name' => $product->merchant->name,
                        'logo' => url($product->merchant->logo),
                        'address' => $product->merchant->address,
                        'province' => $product->merchant->province,
                        'city' => $product->merchant->city,
                        'district' => $product->merchant->districts,
                        'village' => $product->merchant->village
                    ],
                    'price' => $product->price,
                    'stock' => $product->stock,
                    'weight' => $product->weight,
                    'description' => $product->description,
                    'photo' => url($product->photo),
                    'created_at' => $product->created_at,
                    'updated_at' => $product->updated_at,
                ];
                array_push($data, $response);
            }
        }
        return ResponseHelper::paging($data,$request->page,$totalPage);
    }

    public function getProductByMerchantId(Request $request,$id){
        $merchant = Merchant::query()->find($id);
        if($merchant->status!=true){
            return ResponseHelper::badRequest(["Merchant is inactive"], "Invalid Id");
        }
        $query =  Product::query()
            ->where('merchant_id',$id)
            ->where('name','LIKE','%'.$request->name.'%')
            ->where('stock','>',0)
            ->where('status',true)
            ->orderBy('created_at','DESC');
        $totalPage = ceil($query
                ->get()
                ->count() / 10);
        $products = $query->skip($request['page']*10)
            ->take(10)->get();
        $data=[];
        foreach ($products as $product){
            $response=[
                'id' => $product->id,
                'productCategory' => $product->productCategory,
                'name' => $product->name,
                'merchant' => [
                    'id' => $product->merchant->id,
                    'phone' => $product->merchant->user->phone,
                    'name' => $product->merchant->name,
                    'logo' => url($product->merchant->logo),
                    'address' => $product->merchant->address,
                    'province' => $product->merchant->province,
                    'city' => $product->merchant->city,
                    'district' => $product->merchant->districts,
                    'village' => $product->merchant->village
                ],
                'price' => $product->price,
                'stock' => $product->stock,
                'weight' => $product->weight,
                'description' => $product->description,
                'photo' => url($product->photo),
                'created_at' => $product->created_at,
                'updated_at' => $product->updated_at,
            ];
            array_push($data,$response);
        }
        return ResponseHelper::paging($data,$request->page,$totalPage);
    }

    public function getProductById($id){
        $product =Product::query()->find($id);
        if($product->merchant->status!=true){
            return ResponseHelper::badRequest(["Merchant is inactive"], "Invalid Id");
        }
        $response=[
            'id' => $product->id,
            'productCategory' => $product->productCategory,
            'name' => $product->name,
            'merchant' => [
                'id' => $product->merchant->id,
                'phone' => $product->merchant->user->phone,
                'name' => $product->merchant->name,
                'logo' => url($product->merchant->logo),
                'address' => $product->merchant->address,
                'province' => $product->merchant->province,
                'city' => $product->merchant->city,
                'district' => $product->merchant->districts,
                'village' => $product->merchant->village
            ],
            'price' => $product->price,
            'stock' => $product->stock,
            'weight' => $product->weight,
            'description' => $product->description,
            'photo' => url($product->photo),
            'created_at' => $product->created_at,
            'updated_at' => $product->updated_at,
        ];
        return ResponseHelper::ok($response);
    }

    public function getProductMerchant(Request $request){
        $query =  auth()->user()->merchant->product()
            ->where('name','LIKE','%'.$request->name.'%')
            ->where('stock','>',0)
            ->where('status',true)
            ->orderBy('created_at','DESC');
        $totalPage = ceil($query
                ->get()
                ->count() / 10);
        $products = $query->skip($request['page']*10)
            ->take(10)->get();
        $data=[];
        foreach ($products as $product){
            $response=[
                'id' => $product->id,
                'productCategory' => $product->productCategory,
                'name' => $product->name,
                'price' => $product->price,
                'stock' => $product->stock,
                'weight' => $product->weight,
                'description' => $product->description,
                'photo' => url($product->photo),
                'created_at' => $product->created_at,
                'updated_at' => $product->updated_at,
            ];
            array_push($data,$response);
        }
        return ResponseHelper::paging($data,$request->page,$totalPage);
    }

    public function createProduct(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'product_category_id' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|integer',
            'weight' => 'required|integer',
            'description' => 'required',
            'photo' => 'required|image',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $file = $request->photo;
        $image_path = 'storage/product/';
        $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
        $file->move($image_path, $filename);
        auth()->user()->merchant->product()->create([
            'product_category_id' => $request->product_category_id,
            'name' => $request->name,
            'price' => $request->price,
            'stock' => $request->stock,
            'weight' => $request->weight,
            'description' => $request->description,
            'status' => true,
            'photo' => $filename,
        ]);
        return ResponseHelper::ok(true);
    }

    public function updateProduct(Request $request,$id){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|integer',
            'weight' => 'required|integer',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $filename = Product::query()->find($id)->photo;
        if($request->photo){
            $file = $request->photo;
            $image_path = 'storage/product/';
            $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
            $file->move($image_path, $filename);
        }
        auth()->user()->merchant->product()->where('id',$id)->first()->update([
            'name' => $request->name,
            'price' => $request->price,
            'stock' => $request->stock,
            'weight' => $request->weight,
            'description' => $request->description,
            'status' => true,
            'photo' => $filename,
        ]);
        return ResponseHelper::ok(true);
    }

    public function inActiveProduct($id){
        auth()->user()->merchant->product()->where('id',$id)->first()->update([
            'status' => false,
        ]);
        return ResponseHelper::ok(true);
    }
}
