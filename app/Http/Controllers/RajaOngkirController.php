<?php

namespace App\Http\Controllers;

use App\Helper\Client;
use App\Helper\ResponseHelper;
use App\Models\OriginAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RajaOngkirController extends Controller
{
    public function getProvince(){
        $data = Client::rajaOngkirProvince();
        return ResponseHelper::ok($data->rajaongkir->results);
    }

    public function getCity($id){
        $data = Client::rajaOngkirCity($id);
        return ResponseHelper::ok($data->rajaongkir->results);
    }

    public function getSubdistrict($id){
        $data = Client::rajaOngkirSubdistrict($id);
        return ResponseHelper::ok($data->rajaongkir->results);
    }

    public function postOriginAddress(Request $request){
        $validator = Validator::make($request->all(), [
            'province_id' => 'required|integer',
            'city_id' => 'required|integer',
            'subdistrict_id' => 'required|integer',
            'address' => 'required'
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        auth()->user()->merchant->originAddress()->create([
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'subdistrict_id' => $request->subdistrict_id,
            'address'=>$request->address
        ]);
        return ResponseHelper::ok(true);
    }

    public function updateOriginAddress(Request $request){
        $validator = Validator::make($request->all(), [
            'province_id' => 'required|integer',
            'city_id' => 'required|integer',
            'subdistrict_id' => 'required|integer',
            'address' => 'required'
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        auth()->user()->merchant->originAddress()->first()->update([
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'subdistrict_id' => $request->subdistrict_id,
            'address'=>$request->address
        ]);
        return ResponseHelper::ok(true);
    }

    public function checkCost(Request $request){
        $validator = Validator::make($request->all(), [
            'origin' => 'required|numeric',
            'destination' => 'required|numeric',
            'weight' => 'required|integer',
            'courier' => 'required',
            'originType' => 'required',
            'destinationType' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $data=[
            'origin'=>$request->origin,
            'originType'=>$request->originType,
            'destination'=>$request->destination,
            'destinationType'=>$request->destinationType,
            'weight'=>$request->weight,
            'courier'=>$request->courier,
        ];
        $cost=Client::cost($data);
        if($cost->rajaongkir->status->code==400){
            return ResponseHelper::serviceUnavailable("data tidak valid");
        }
        $response=[
            'origin_details'=>$cost->rajaongkir->origin_details,
            'destination_details'=>$cost->rajaongkir->destination_details,
            'cost'=>$cost->rajaongkir->results
        ];
        return ResponseHelper::ok($response);
    }

    public function getOriginAddress(){
        $address = auth()->user()->merchant->originAddress()->first();
        if(!$address){
            return ResponseHelper::badRequest(['Silahkan masukan alamat kirim toko'],'Validation Error');
        }
        $province = Client::rajaOngkirProvinceById($address->province_id);
        $city = Client::rajaOngkirCityById($address->city_id);
        $subdistrict = Client::rajaOngkirSubdistrictById($address->subdistrict_id);
        $response=[
            'province'=>$province->rajaongkir->results,
            'city'=>$city->rajaongkir->results,
            'subdistrict'=>$subdistrict->rajaongkir->results,
            'address' => $address->address
        ];
        return ResponseHelper::ok($response);
    }

    public function getOriginAddressByMerchantId($id){
        $address = OriginAddress::query()->where('merchant_id',$id)->first();
        if(!$address){
            return ResponseHelper::badRequest(['Toko belum memasukan alamat kirim'],'Validation Error');
        }
        $province = Client::rajaOngkirProvinceById($address->province_id);
        $city = Client::rajaOngkirCityById($address->city_id);
        $subdistrict = Client::rajaOngkirSubdistrictById($address->subdistrict_id);
        $response=[
            'merchant'=>[
                'id'=>$address->merchant->id,
                'name'=>$address->merchant->name,
                'logo'=>url($address->merchant->logo)
            ],
            'province'=>$province->rajaongkir->results,
            'city'=>$city->rajaongkir->results,
            'subdistrict'=>$subdistrict->rajaongkir->results,
            'address' => $address->address
        ];
        return ResponseHelper::ok($response);
    }
}
