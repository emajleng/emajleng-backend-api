<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('merchant_id')->nullable(false);
            $table->bigInteger('product_category_id')->nullable(false);
            $table->string('name')->nullable(false);
            $table->string('photo')->nullable(false);
            $table->text('description')->nullable(false);
            $table->integer('stock')->nullable(false);
            $table->bigInteger('price')->nullable(false);
            $table->integer('weight')->nullable(false);
            $table->boolean('status')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_product');
    }
}
