<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/test', 'ExampleController@testNotification');

$router->group(['prefix' => 'auth'], function() use ($router) {
    $router->post('/register', 'AuthController@register');
    $router->post('/login', 'AuthController@login');
    $router->group(['middleware' => 'auth'], function() use ($router) {
        $router->post('/logout', 'AuthController@logout');
    });
});

$router->group(['prefix' => 'raja-ongkir'], function() use ($router) {
    $router->get('/province', 'RajaOngkirController@getProvince');
    $router->get('/city/{id}', 'RajaOngkirController@getCity');
    $router->get('/subdistrict/{id}', 'RajaOngkirController@getSubdistrict');
    $router->post('/cost', 'RajaOngkirController@checkCost');
    $router->group(['middleware' => 'auth'], function() use ($router) {
        $router->post('/origin-address', 'RajaOngkirController@postOriginAddress');
        $router->put('/origin-address', 'RajaOngkirController@updateOriginAddress');
        $router->get('/origin-address', 'RajaOngkirController@getOriginAddress');
        $router->get('/origin-address/{id}', 'RajaOngkirController@getOriginAddressByMerchantId');
    });
});

$router->group(['prefix' => 'public'], function() use ($router) {
    $router->get('/product_category', 'PublicController@getProductCategory');
    $router->get('/payment_method', 'PublicController@getPaymentMethod');
    $router->get('/province', 'PublicController@getProvince');
    $router->get('/news', 'PublicController@getNews');
    $router->get('/news/{id}', 'PublicController@getNewsById');
    $router->get('/city/{id}', 'PublicController@getCity');
    $router->get('/district/{id}', 'PublicController@getDistrict');
    $router->get('/village/{id}', 'PublicController@getVillage');
    $router->get('/banner', 'PublicController@getBanner');
    $router->get('/menu', 'PublicController@getMenu');
    $router->get('/provider', 'PublicController@getProvider');
    $router->get('/courier', 'PublicController@getCourier');
    $router->get('/admin-fee', 'PublicController@getAdminFee');
});

$router->group(['prefix' => 'user','middleware' => 'auth'], function() use ($router) {
    $router->group(['prefix' => 'merchant'], function() use ($router) {
        $router->get('/', 'UserController@getMerchant');
        $router->post('/', 'UserController@createMerchant');
        $router->post('/update', 'UserController@updateMerchant');
    });
    $router->get('/', 'UserController@getUser');
    $router->put('/', 'UserController@updateUser');
});

$router->group(['prefix' => 'product'], function() use ($router) {
    $router->get('/product/all', 'ProductController@getAllProduct');
    $router->get('/category/{id}', 'ProductController@getProductByCategory');
    $router->get('/{id}', 'ProductController@getProductById');
    $router->get('/merchant/{id}', 'ProductController@getProductByMerchantId');
    $router->group(['middleware' => 'auth'], function() use ($router) {
        $router->post('/', 'ProductController@createProduct');
        $router->post('/{id}', 'ProductController@updateProduct');
        $router->delete('/{id}', 'ProductController@inActiveProduct');
        $router->get('/', 'ProductController@getProductMerchant');
    });
});

$router->group(['prefix' => 'user_bank','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'UserBankController@getUserBank');
    $router->post('/', 'UserBankController@createUserBank');
    $router->put('/{id}', 'UserBankController@updateUserBank');
    $router->delete('/{id}', 'UserBankController@deleteUserBank');
});

$router->group(['prefix' => 'chat','middleware' => 'auth'], function() use ($router) {
    $router->post('/', 'ChatController@postChat');
    $router->get('/merchant', 'ChatController@getChatMerchant');
    $router->get('/user', 'ChatController@getChatUser');
    $router->get('/message', 'ChatController@getChatMessage');
});

$router->group(['prefix' => 'cart','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'CartController@getCart');
    $router->post('/', 'CartController@addProductToCart');
    $router->put('/{id}', 'CartController@updateProductCart');
    $router->delete('/{id}', 'CartController@deleteCart');
});

$router->group(['prefix' => 'shipping-destination','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'ShippingDestinationController@index');
    $router->post('/', 'ShippingDestinationController@create');
    $router->put('/{id}', 'ShippingDestinationController@update');
    $router->delete('/{id}', 'ShippingDestinationController@delete');
});

$router->group(['prefix' => 'topup','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'TopUpController@getTopUp');
    $router->post('/', 'TopUpController@topUp');
});

$router->group(['prefix' => 'withdraw','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'WithdrawController@getWithdraw');
    $router->post('/', 'WithdrawController@withdraw');
});

$router->post('/webhook', 'WebHookController@index');

$router->group(['prefix' => 'transaction-ppob','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'PpobTransactionController@getTransaction');
    $router->post('/topup', 'PpobTransactionController@topup');
    $router->post('/pasca', 'PpobTransactionController@pasca');
    $router->post('/pln/inquiry', 'PpobTransactionController@inquiry');
});

$router->group(['prefix' => 'transaction','middleware' => 'auth'], function() use ($router) {
    $router->post('/', 'TransactionController@createTransaction');
    $router->get('/', 'TransactionController@getUserTransaction');
    $router->get('/merchant', 'TransactionController@getMerchantTransaction');
    $router->group(['prefix' => 'status'], function() use ($router) {
        $router->post('/confirmation/{id}', 'TransactionController@confirmationPayment');
        $router->post('/process/{id}', 'TransactionController@processTransaction');
        $router->post('/reject/{id}', 'TransactionController@rejectTransaction');
        $router->post('/deliver/{id}', 'TransactionController@deliverProduct');
        $router->post('/finish/{id}', 'TransactionController@finishTransaction');
        $router->post('/cancel/{id}', 'TransactionController@userCancelTransaction');
    });
});
