<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = [
        'user_id',
        'merchant_id',
        'payment_method_id',
        'courier_id',
        'courier_service',
        'amount',
        'unique_code',
        'detail',
        'status',
        'postal_fee',
        'delivery_time',
        'payment_expired',
        'airway_bill',
        'origin_details',
        'destination_details',
        'payment_confirmation'

    ];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function merchant(){
        return $this->belongsTo('App\Models\Merchant','merchant_id','id');
    }

    public function payment_method(){
        return $this->belongsTo('App\Models\PaymentMethod','payment_method_id','id');
    }

    public function courier(){
        return $this->belongsTo('App\Models\Courier','courier_id','id');
    }
}
