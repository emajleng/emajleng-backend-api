<?php
namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\ShippingDestination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ShippingDestinationController extends Controller
{
    public function index(){
        $destination = auth()->user()->shippingDestination()->get();
        return ResponseHelper::ok($destination);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'address_name' => 'required',
            'recipient_name' => 'required',
            'destination_detail' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }

        auth()->user()->shippingDestination()->create([
            'address_name'=>$request->address_name,
            'recipient_name'=>$request->recipient_name,
            'destination_detail'=>$request->destination_detail
        ]);
        return ResponseHelper::ok(true);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'address_name' => 'required',
            'recipient_name' => 'required',
            'destination_detail' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }

        auth()->user()->shippingDestination()->where('id',$id)->first()->update([
            'address_name'=>$request->address_name,
            'recipient_name'=>$request->recipient_name,
            'destination_detail'=>$request->destination_detail
        ]);
        return ResponseHelper::ok(true);
    }

    public function delete($id){
        $destination = ShippingDestination::query()->find($id);
        $destination->delete();
        return ResponseHelper::ok(true);
    }

}
