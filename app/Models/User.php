<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable, HasFactory, Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'village_id',
        'district_id',
        'city_id',
        'province_id',
        'postal_code',
        'balance',
        'password',
        'fcm_token',
        'latest_token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','fcm_token','latest_token'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function routeNotificationForFcm()
    {
        return $this->fcm_token;
    }

    public function routeNotificationForMail()
    {
        return $this->email;
    }

    public function cart()
    {
        return $this->hasMany('App\Models\Cart', 'user_id');
    }

    public function chat()
    {
        return $this->hasMany('App\Models\Chat', 'user_id');
    }

    public function topUp()
    {
        return $this->hasMany('App\Models\TopUp', 'user_id');
    }

    public function province(){
        return $this->belongsTo('App\Models\Province','province_id','id');
    }

    public function city(){
        return $this->belongsTo('App\Models\City','city_id','id');
    }

    public function districts(){
        return $this->belongsTo('App\Models\Districts','district_id','id');
    }

    public function village(){
        return $this->belongsTo('App\Models\Village','village_id','id');
    }

    public function merchant()
    {
        return $this->hasOne('App\Models\Merchant', 'user_id');
    }

    public function userBank()
    {
        return $this->hasMany('App\Models\UserBank', 'user_id');
    }

    public function ppobTransaction()
    {
        return $this->hasMany('App\Models\PpobTransaction', 'user_id');
    }

    public function transaction()
    {
        return $this->hasMany('App\Models\Transaction', 'user_id');
    }

    public function transactionEmoney()
    {
        return $this->hasMany('App\Models\TransactionEmoney', 'user_id');
    }

    public function transactionPulsa()
    {
        return $this->hasMany('App\Models\TransactionPulsa', 'user_id');
    }

    public function transactionPacketData()
    {
        return $this->hasMany('App\Models\TransactionPacketData', 'user_id');
    }

    public function transactionPlnPrepaid()
    {
        return $this->hasMany('App\Models\TransactionPlnPrepaid', 'user_id');
    }

    public function transactionPlnPostpaid()
    {
        return $this->hasMany('App\Models\TransactionPlnPostpaid', 'user_id');
    }

    public function shippingDestination()
    {
        return $this->hasMany('App\Models\ShippingDestination', 'user_id');
    }
}
