<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    protected $table = 'merchants';

    protected  $fillable = [
        'name',
        'logo',
        'latitude',
        'longitude',
        'address',
        'village_id',
        'district_id',
        'city_id',
        'province_id',
        'postal_code',
        'status',
        'siup_image',
        'siup_status'
    ];

    public function chat()
    {
        return $this->hasMany('App\Models\Chat', 'merchant_id');
    }

    public function province(){
        return $this->belongsTo('App\Models\Province','province_id','id');
    }

    public function city(){
        return $this->belongsTo('App\Models\City','city_id','id');
    }

    public function districts(){
        return $this->belongsTo('App\Models\Districts','district_id','id');
    }

    public function village(){
        return $this->belongsTo('App\Models\Village','village_id','id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function product()
    {
        return $this->hasMany('App\Models\Product', 'merchant_id');
    }

    public function originAddress()
    {
        return $this->hasMany('App\Models\OriginAddress', 'merchant_id');
    }

    public function transaction()
    {
        return $this->hasMany('App\Models\Transaction', 'merchant_id');
    }
}
