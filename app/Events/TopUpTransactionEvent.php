<?php

namespace App\Events;

class TopUpTransactionEvent extends Event
{
    public $topUpTransaction;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($denomId, $userId, $amount, $customerNumber, $status, $clientResponse, $type, $refId, $balance, $adminFee)
    {
        $this->topUpTransaction['denom_id'] = $denomId;
        $this->topUpTransaction['user_id'] = $userId;
        $this->topUpTransaction['amount'] = $amount;
        $this->topUpTransaction['customer_number'] = $customerNumber;
        $this->topUpTransaction['status'] = $status;
        $this->topUpTransaction['client_response'] = $clientResponse;
        $this->topUpTransaction['type'] = $type;
        $this->topUpTransaction['ref_id'] = $refId;
        $this->topUpTransaction['balance'] = $balance;
        $this->topUpTransaction['admin_fee'] = $adminFee;
    }
}
