<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table='payment_methods';
    protected $fillable = [
        'bank_name',
        'account_name',
        'account_number'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    public function topUp()
    {
        return $this->hasMany('App\Models\TopUp', 'payment_method_id');
    }

    public function transaction()
    {
        return $this->hasMany('App\Models\Transaction', 'payment_method_id');
    }
}
