<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $table = 'withdraws';
    protected $fillable = [
        'user_bank_id',
        'amount',
        'status'
    ];

    public function userBank(){
        return $this->belongsTo('App\Models\UserBank','user_bank_id','id');
    }
}
