<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourierService extends Model
{
    protected $table = 'courier_services';

    protected $fillable = [
        'courier_id',
        'name'
    ];

    public function courier(){
        return $this->belongsTo('App\Models\Courier','courier_id','id');
    }
}
