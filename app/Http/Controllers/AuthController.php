<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request){
        $credentials = request(['email', 'password']);
        if (! $token = app('auth')->attempt($credentials)) {
            return ResponseHelper::unauthorized("Email atau password yang anda masukan salah!");
        }
        $user = User::query()->find(auth()->user()->id);
        $user->fcm_token = $request->fcm_token;
        $user->latest_token = $token;
        $user->save();
        $response = [
            'token' => $token,
            'token_type' => 'bearer',
        ];
        return ResponseHelper::ok($response);
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|unique:users',
            'email' => 'required|unique:users',
            'address' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        User::query()->create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'district_id' => $request->district_id,
            'village_id' => $request->village_id,
            'balance' => 0,
            'password'=>app('hash')->make($request->password)
        ]);
        return ResponseHelper::ok(true);
    }

    public function logout(){
        app('auth')->logout();
        return ResponseHelper::ok(true);
    }
}
