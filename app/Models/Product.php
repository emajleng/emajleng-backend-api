<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected  $fillable = [
        'merchant_id',
        'product_category_id',
        'name',
        'description',
        'photo',
        'weight',
        'stock',
        'price',
        'status'
    ];

    protected $hidden = [
        'status'
    ];

    public function productCategory(){
        return $this->belongsTo('App\Models\ProductCategory','product_category_id','id');
    }

    public function merchant(){
        return $this->belongsTo('App\Models\Merchant','merchant_id','id');
    }

    public function cart()
    {
        return $this->hasMany('App\Models\Cart', 'product_id');
    }
}
