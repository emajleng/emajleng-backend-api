<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table= 'menu';

    protected $fillable = [
        'name',
        'status',
        'icon',
        'route'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];
}
