<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WithdrawController extends Controller
{
    public function getWithdraw(Request $request){
        $userBanks = auth()->user()->userBank()->get();
        $userBankId = [];
        foreach ($userBanks as $userBank){
            array_push($userBankId,$userBank->id);
        }
        $data = Withdraw::query()->whereIn('user_bank_id',$userBankId);
        $withdraws = $data->orderBy('updated_at', 'desc')
            ->skip($request['page']*10)
            ->take(10)
            ->get();
        foreach($withdraws as $withdraw){
            $withdraw['user_bank'] = $withdraw->userBank;
        }
        $totalPage = ceil($data
                ->get()
                ->count() / 10);
        return ResponseHelper::paging($withdraws, $request['page'], $totalPage);
    }

    public function withdraw(Request $request){
        $validator = Validator::make($request->all(), [
            'user_bank_id' => 'required|integer|exists:user_banks,id',
            'amount' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        if(auth()->user()->balance<$request->amount){
            return ResponseHelper::badRequest(['Saldo anda tidak cukup'], "Saldo anda tidak cukup");
        }
        Withdraw::query()->create([
            'user_bank_id' => $request->user_bank_id,
            'amount' => $request->amount,
            'status' => 'PENDING'
        ]);
        $this->minusBalance($request->amount);
        return ResponseHelper::ok(true);
    }

    public function minusBalance($amount){
        $user= auth()->user();
        $user->balance -= $amount;
        $user->save();
        return true;
    }
}
