<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopUp extends Model
{
    protected $table='top_ups';
    protected $fillable = [
        'user_id',
        'payment_method_id',
        'amount',
        'unique_code',
        'status',
        'payment_expired'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function paymentMethod(){
        return $this->belongsTo('App\Models\PaymentMethod','payment_method_id','id');
    }
}
