<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionEmoney extends Model
{
    protected $table= 'transaction_emoney';

    protected $fillable = [
        'user_id',
        'denom_id',
        'amount',
        'customer_number',
        'status',
        'client_respone',
        'ref_id',
        'balance',
        'admin_fee'
    ];

    protected $hidden = [
        'client_response'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function denom(){
        return $this->belongsTo('App\Models\Denom','denom_id','id');
    }
}
