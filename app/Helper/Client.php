<?php

namespace App\Helper;
use Illuminate\Support\Facades\Http;

class Client{
    public static function digiflazz($data,$url)
    {
        $digiflazz = Http::post(env('UrlDigiflazz').$url, $data);
        $response = json_decode($digiflazz->body());
        return $response;
    }

    public static function rajaOngkirProvince()
    {
        $request = Http::withHeaders([
            'key' => env('RajaOngkirKey'),
        ])->get(env('RajaOngkirUrl').'/province');
        $response = json_decode($request->body());
        return $response;
    }

    public static function rajaOngkirProvinceById($id)
    {
        $request = Http::withHeaders([
            'key' => env('RajaOngkirKey'),
        ])->get(env('RajaOngkirUrl').'/province?id='.$id);
        $response = json_decode($request->body());
        return $response;
    }

    public static function rajaOngkirCity($province_id)
    {
        $request = Http::withHeaders([
            'key' => env('RajaOngkirKey'),
        ])->get(env('RajaOngkirUrl').'/city?province='.$province_id);
        $response = json_decode($request->body());
        return $response;
    }

    public static function rajaOngkirCityById($id)
    {
        $request = Http::withHeaders([
            'key' => env('RajaOngkirKey'),
        ])->get(env('RajaOngkirUrl').'/city?id='.$id);
        $response = json_decode($request->body());
        return $response;
    }

    public static function rajaOngkirSubdistrict($city_id)
    {
        $request = Http::withHeaders([
            'key' => env('RajaOngkirKey'),
        ])->get(env('RajaOngkirUrl').'/subdistrict?city='.$city_id);
        $response = json_decode($request->body());
        return $response;
    }

    public static function rajaOngkirSubdistrictById($id)
    {
        $request = Http::withHeaders([
            'key' => env('RajaOngkirKey'),
        ])->get(env('RajaOngkirUrl').'/subdistrict?id='.$id);
        $response = json_decode($request->body());
        return $response;
    }

    public static function cost($data)
    {
        $request = Http::withHeaders([
            'key' => env('RajaOngkirKey'),
        ])->post(env('RajaOngkirUrl').'/cost',$data);
        $response = json_decode($request->body());
        return $response;
    }
}
