<?php

namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use App\Models\Log;
use App\Models\TransactionEmoney;
use App\Models\TransactionPacketData;
use App\Models\TransactionPlnPostpaid;
use App\Models\TransactionPlnPrepaid;
use App\Models\TransactionPulsa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebHookController extends Controller
{
    public function index(Request $request)
    {
        sleep(7);
        $data=json_decode($request->getContent());
        Log::query()->create([
            'value'=>json_encode($request->header('X-Hub-Signature'))
        ]);
        Log::query()->create([
            'value'=>json_encode($data->data)
        ]);
        $postData = file_get_contents('php://input');
        $secret=env('WebHookSecret');
        $signature = hash_hmac('sha1', $postData, $secret);
        if ($request->header('X-Hub-Signature') == 'sha1='.$signature) {
            Log::query()->create([
                'value'=>'Wrong signature'
            ]);
            return 'wrong signature';
        }
        DB::beginTransaction();
        try{
            $type=substr($data->data->ref_id,9);
            if($type=='PULSA'){
                $query = TransactionPulsa::query();
            }elseif($type=='DATA'){
                $query = TransactionPacketData::query();
            }elseif($type=='EMONEY'){
                $query = TransactionEmoney::query();
            }elseif($type=='PLN'){
                $query = TransactionPlnPrepaid::query();
            }else{
                $query = TransactionPlnPostpaid::query();
            }
            $query = $query->where('ref_id',$data->data->ref_id);
            $transaction = $query->first();
            $lastStatusTransaction = $transaction->status;
            $transaction->status=$data->data->status;
            if($data->data->rc!="00"&&$data->data->rc!="03"&&$data->data->rc!="99"){
                $amountTransaction=$query->first();
                $transaction->balance+=$amountTransaction->amount;
            }
            $transaction->client_response=json_encode($data->data);
            if($type=='PLN'){
                $transaction->token_pln=substr($data->data->sn, 0, 24);
            }
            $transaction->save();
            $transactionUser = $query->first();
            if($data->data->rc!="00"&&$data->data->rc!="03"&&$data->data->rc!="99"){
                if($lastStatusTransaction=='Pending') {
                    $user = User::query()->find($transactionUser->user_id);
                    $user->balance += $transactionUser->amount;
                    $user->save();
                }
            }
        }catch(\Exception $e){
            DB::rollBack();
            Log::query()->create([
                'value'=>$e
            ]);
            return 'internal server error';
        }
        DB::commit();
        $notif = [
            'type' => $type,
            'ref_id' => $data->data->ref_id,
            'status' => $data->data->status,
        ];
        event(new NotificationEvent($notif, $transactionUser));
        return true;
    }
}
