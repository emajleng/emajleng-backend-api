<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    protected $table = 'user_banks';
    protected $fillable = [
        'user_id',
        'bank_name',
        'account_name',
        'account_number',
        'status'
    ];

    protected $hidden = [
        'status','created_at','updated_at'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function withdraw(){
        return $this->hasMany('App\Models\Withdraw', 'user_bank_id');
    }
}
