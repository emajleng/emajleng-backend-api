<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable(false);
            $table->bigInteger('merchant_id')->nullable(false);
            $table->bigInteger('payment_method_id')->nullable(false);
            $table->bigInteger('courier_id')->nullable(false);
            $table->string('courier_service')->nullable(false);
            $table->bigInteger('amount')->nullable(false);
            $table->integer('unique_code')->nullable(false);
            $table->text('detail')->nullable(false);
            $table->string('status')->nullable(false);
            $table->integer('postal_fee')->nullable(false);
            $table->integer('delivery_time')->nullable(false);
            $table->dateTime('payment_expired');
            $table->string('airway_bill');
            $table->text('origin_details');
            $table->text('destination_details');
            $table->string('payment_confirmation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_transaction');
    }
}
