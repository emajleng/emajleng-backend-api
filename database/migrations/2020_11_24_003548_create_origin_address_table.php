<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOriginAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('origin_address', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('merchant_id')->nullable(false);
            $table->integer('province_id')->nullable(false);
            $table->integer('city_id')->nullable(false);
            $table->integer('subdistrict_id')->nullable(false);
            $table->string('address')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('origin_address');
    }
}
