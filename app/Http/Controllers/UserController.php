<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Merchant;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function getUser(){
        if(auth()->user()->merchant){
            $merchant = true;
        }else{
            $merchant = false;
        }
        $response = [
            'id' => auth()->user()->id,
            'name' => auth()->user()->name,
            'phone' => auth()->user()->phone,
            'email' => auth()->user()->email,
            'address' => auth()->user()->address,
            'balance' => auth()->user()->balance,
            'province' => auth()->user()->province,
            'city' => auth()->user()->city,
            'district' => auth()->user()->districts,
            'village' => auth()->user()->village,
            'merchant' => $merchant
        ];
        return ResponseHelper::ok($response);
    }

    public function createMerchant(Request $request){
        if(auth()->user()->merchant){
            return ResponseHelper::badRequest(['Anda sudah membuat merchant'],'you already have merchant');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'logo' => 'required|image',
            'siup_image' => 'required|image',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $file = $request->logo;
        $image_path = 'storage/image/merchant/logo/';
        $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
        $file->move($image_path, $filename);

        $siup_file = $request->siup_image;
        $siup_image_path = 'storage/image/merchant/siup/';
        $siup_filename = $siup_image_path.Carbon::now()->unix() . '.' . $siup_file->getClientOriginalExtension();
        $siup_file->move($siup_image_path, $siup_filename);
        auth()->user()->merchant()->create([
            'name' => $request->name,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'district_id' => $request->district_id,
            'village_id' => $request->village_id,
            'logo' => $filename,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'siup_image' => $siup_filename,
            'siup_status' => false,
            'status' => false
        ]);
        return ResponseHelper::ok(true);
    }

    public function getMerchant(){
        if(!auth()->user()->merchant){
            return ResponseHelper::badRequest(['Anda belum membuat merchant'],'Anda belum membuat merchant');
        }
        $response = [
            'id' => auth()->user()->merchant->id,
            'name' => auth()->user()->merchant->name,
            'logo' => url(auth()->user()->merchant->logo),
            'address' => auth()->user()->merchant->address,
            'province' => auth()->user()->merchant->province,
            'city' => auth()->user()->merchant->city,
            'district' => auth()->user()->merchant->districts,
            'village' => auth()->user()->merchant->village,
            'siup_image' => url(auth()->user()->merchant->siup_image),
            'siup_status' => auth()->user()->merchant->siup_status,
            'status' => auth()->user()->merchant->status
        ];
        return ResponseHelper::ok($response);
    }

    public function updateUser(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        User::query()->find(auth()->user()->id)->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'district_id' => $request->district_id,
            'village_id' => $request->village_id,
        ]);
        return ResponseHelper::ok(true);
    }

    public function updateMerchant(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $filename = auth()->user()->merchant->logo;
        if($request->logo){
            $file = $request->logo;
            $image_path = 'storage/image/merchant/logo/';
            $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
            $file->move($image_path, $filename);
        }
        $siup_filename = auth()->user()->merchant->siup_image;
        if($request->siup_image){
            $siup_file = $request->siup_image;
            $siup_image_path = 'storage/image/merchant/siup/';
            $siup_filename = $siup_image_path.Carbon::now()->unix() . '.' . $siup_file->getClientOriginalExtension();
            $siup_file->move($siup_image_path, $siup_filename);
        }
        auth()->user()->merchant()->update([
            'name' => $request->name,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'district_id' => $request->district_id,
            'village_id' => $request->village_id,
            'logo' => $filename,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'siup_image' => $siup_filename,
        ]);
        return ResponseHelper::ok(true);
    }

    public function getListMerchant(Request $request){
        $query = Merchant::query()
            ->where('name','Like','%'.$request->name.'%')
            ->where('status',true)
            ->orderBy('created_at','DESC');
        $totalPage = ceil($query
                ->get()
                ->count() / 10);
        $merchants = $query->skip($request['page']*10)
            ->take(10)->get();
        foreach ($merchants as $merchant){
            $merchant['logo'] = url($merchant->logo);
            $merchant['province'] = $merchant->province;
            $merchant['city'] = $merchant->city;
            $merchant['district'] = $merchant->districts;
            $merchant['village'] = $merchant->village;
        }
        return ResponseHelper::paging($merchants,$request->page,$totalPage);
    }
}
