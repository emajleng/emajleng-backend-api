<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OriginAddress extends Model
{
    protected $table = 'origin_address';

    protected $fillable = [
        'merchant_id',
        'province_id',
        'city_id',
        'subdistrict_id',
        'address'
    ];

    public function merchant(){
        return $this->belongsTo('App\Models\Merchant','merchant_id','id');
    }
}
