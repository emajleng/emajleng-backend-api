<?php

namespace App\Listeners;

use App\Events\TopUpTransactionEvent;
use App\Models\TransactionEmoney;
use App\Models\TransactionPacketData;
use App\Models\TransactionPulsa;
use Illuminate\Contracts\Queue\ShouldQueue;

class TopUpTransactionListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\TopUpTransactionEvent  $event
     * @return void
     */
    public function handle(TopUpTransactionEvent $event)
    {
        //
        if($event->topUpTransaction['type']=='PULSA'){
            $topUpTransaction = new TransactionPulsa();
        }elseif($event->topUpTransaction['type']=='DATA'){
            $topUpTransaction = new TransactionPacketData();
        }elseif($event->topUpTransaction['type']=='EMONEY'){
            $topUpTransaction = new TransactionEmoney();
        }
        $topUpTransaction['denom_id'] = $event->topUpTransaction['denom_id'];
        $topUpTransaction['user_id'] = $event->topUpTransaction['user_id'];
        $topUpTransaction['amount'] = $event->topUpTransaction['amount'];
        $topUpTransaction['customer_number'] = $event->topUpTransaction['customer_number'];
        $topUpTransaction['status'] = $event->topUpTransaction['status'];
        $topUpTransaction['client_response'] = $event->topUpTransaction['client_response'];
        $topUpTransaction['ref_id'] = $event->topUpTransaction['ref_id'];
        $topUpTransaction['balance'] = $event->topUpTransaction['balance'];
        $topUpTransaction['admin_fee'] = $event->topUpTransaction['admin_fee'];
        $topUpTransaction->save();
    }
}
