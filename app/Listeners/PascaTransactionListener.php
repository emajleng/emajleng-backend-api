<?php

namespace App\Listeners;

use App\Events\PascaTransactionEvent;
use App\Models\TransactionPlnPostpaid;
use App\Models\TransactionPlnPrepaid;
use Illuminate\Contracts\Queue\ShouldQueue;

class PascaTransactionListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\PascaTransactionEvent  $event
     * @return void
     */
    public function handle(PascaTransactionEvent $event)
    {
        //
        if($event->pascaTransaction['type']=='PLN_POSTPAID'){
            $pascaTransaction = new TransactionPlnPostpaid();
        }else{
            $pascaTransaction = new TransactionPlnPrepaid();
        }
        $pascaTransaction['denom_id'] = $event->pascaTransaction['denom_id'];
        $pascaTransaction['user_id'] = $event->pascaTransaction['user_id'];
        $pascaTransaction['amount'] = $event->pascaTransaction['amount'];
        $pascaTransaction['customer_number'] = $event->pascaTransaction['customer_number'];
        $pascaTransaction['customer_name'] = $event->pascaTransaction['customer_name'];
        $pascaTransaction['status'] = $event->pascaTransaction['status'];
        $pascaTransaction['client_response'] = $event->pascaTransaction['client_response'];
        $pascaTransaction['tarif'] = $event->pascaTransaction['tarif'];
        if($event->pascaTransaction['type']=='PLN_POSTPAID'){
            $pascaTransaction['period'] = $event->pascaTransaction['period'];
        }else{
            $pascaTransaction['token_pln'] = $event->pascaTransaction['period'];
        }
        $pascaTransaction['voltage'] = $event->pascaTransaction['voltage'];
        $pascaTransaction['ref_id'] = $event->pascaTransaction['ref_id'];
        $pascaTransaction['balance'] = $event->pascaTransaction['balance'];
        $pascaTransaction['admin_fee'] = $event->pascaTransaction['admin_fee'];
        $pascaTransaction->save();
    }
}
