<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\TopUpTransactionEvent::class => [
            \App\Listeners\TopUpTransactionListener::class,
        ],
        \App\Events\PascaTransactionEvent::class => [
            \App\Listeners\PascaTransactionListener::class,
        ],
        \App\Events\NotificationEvent::class => [
            \App\Listeners\NotificationListener::class,
        ],
        \App\Events\TransactionStatusEvent::class => [
            \App\Listeners\TransactionStatusListener::class,
        ],
        \App\Events\ChatEvent::class => [
            \App\Listeners\ChatListener::class,
        ]
    ];
}
