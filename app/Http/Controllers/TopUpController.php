<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TopUpController extends Controller
{
    public function getTopUp(Request $request){
        $data = auth()->user()->topUp();
        $transactions = $data->orderBy('updated_at', 'desc')
            ->skip($request['page']*10)
            ->take(10)
            ->get();
        foreach($transactions as $transaction){
            $transaction['payment_method'] = $transaction->paymentMethod;
        }
        $totalPage = ceil($data
                ->get()
                ->count() / 10);
        return ResponseHelper::paging($transactions, $request['page'], $totalPage);
    }

    public function topUp(Request $request){
        $validator = Validator::make($request->all(), [
            'payment_method_id' => 'required|integer|exists:payment_methods,id',
            'amount' => 'required|integer|min:50000'
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $unique_code = mt_rand(001, 999);
        while($this->checkUniqueCode($unique_code)) {
            if($this->checkUniqueCode($unique_code)){
                $unique_code = mt_rand(001, 999);
            }else{
                break;
            }
        }
        $payment_expired = Carbon::now()->addDay();
        $topUp = auth()->user()->topUp()->create([
            'payment_method_id' => $request->payment_method_id,
            'amount' => $request->amount,
            'status' => 'PENDING',
            'unique_code' => $unique_code,
            'payment_expired' => $payment_expired,
        ]);
        return ResponseHelper::ok($topUp);
    }

    public function checkUniqueCode($unique_code){
        $data = auth()->user()->topUp->where('status','PENDING')->where('unique_code',$unique_code)->first();
        return $data;
    }
}
