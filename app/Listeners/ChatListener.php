<?php

namespace App\Listeners;

use App\Events\ChatEvent;
use App\Models\User;
use App\Notifications\ChatNotification;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChatListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ChatEvent $event
     * @return void
     */
    public function handle(ChatEvent $event)
    {
        $data = $event->data['data'];
        $user = User::query()->find($event->data['user_id']);
        $user->notify(new ChatNotification($data));
    }
}
