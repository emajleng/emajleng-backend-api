<?php
namespace App\Http\Controllers;

use App\Events\ChatEvent;
use App\Helper\ResponseHelper;
use App\Models\Chat;
use App\Models\Merchant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    public function getChatUser(){
        $chats = Chat::query()
            ->where('user_id',auth()->user()->id)
            ->orderBy('created_at','DESC')
            ->get()
            ->groupBy('merchant_id');
        $data = [];
        foreach ($chats as $chats){
            foreach ($chats as $chat){
                $id = $chat->merchant->id;
                $name = $chat->merchant->name;
                $logo = $chat->merchant->logo;
                $user_id = $chat->user_id;
                $merchant_id = $chat->merchant_id;
            }
            $response = [
                'id' => $id,
                'user_id' => $user_id,
                'merchant_id' => $merchant_id,
                'name' => $name,
                'logo' => $logo
            ];
            array_push($data,$response);
        }
        return ResponseHelper::ok($data);
    }

    public function getChatMessage(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'merchant_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $chats = Chat::query()
            ->where('user_id',$request->user_id)
            ->where('merchant_id',$request->merchant_id)
            ->orderBy('created_at','DESC')
            ->get();
        return ResponseHelper::ok($chats);
    }

    public function getChatMerchant(){
        if(!auth()->user()->merchant){
            return ResponseHelper::badRequest(['Anda belum membuat toko'],"Validation required");
        }
        $chats = Chat::query()
            ->where('merchant_id',auth()->user()->merchant->id)
            ->orderBy('created_at','DESC')
            ->get()
            ->groupBy('user_id');
        $data = [];
        foreach ($chats as $chats){
            foreach ($chats as $chat){
                $id = $chat->user->id;
                $name = $chat->user->name;
                $user_id = $chat->user_id;
                $merchant_id = $chat->merchant_id;
            }
            $response = [
                'id' => $id,
                'name' => $name,
                'user_id' => $user_id,
                'merchant_id' => $merchant_id
            ];
            array_push($data,$response);
        }
        return ResponseHelper::ok($data);
    }

    public function postChat(Request $request){
        $validationType = array('MERCHANT','USER');
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'merchant_id' => 'required',
            'message' => 'required',
            'to' => 'required|in:' . implode(',', $validationType),
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }

        Chat::query()->create([
            'user_id' => $request->user_id,
            'merchant_id' => $request->merchant_id,
            'message' => $request->message,
            'recipient' => $request->to
        ]);

        $merchant = Merchant::query()->find($request->merchant_id);
        $user = User::query()->find($request->user_id);
        if($request->to=='USER'){
            $data=[
                'title' => $merchant->name,
                'notification' => $request->message,
            ];
            event(new ChatEvent($data, $request->user_id));
        }else{
            $data=[
                'title' => $user->name,
                'notification' => $request->message,
                'user_id' => $request->user_id,
                'merchant_id' => $request->merchant_id,
                'message' => $request->message,
                'recipient' => $request->to
            ];
            event(new ChatEvent($data, $merchant->user_id));
        }
        return ResponseHelper::ok(true);
    }
}
