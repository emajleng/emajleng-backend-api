<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Denom extends Model
{
    protected $table = 'denom';

    protected $fillable = [
        'provider_id',
        'amount',
        'code',
        'price',
        'admin_fee',
        'status',
        'total'
    ];

    public function provider(){
        return $this->belongsTo('App\Models\Provider','provider_id','id');
    }

    public function TransactionEmoney()
    {
        return $this->hasMany('App\Models\TransactionEmoney', 'denom_id');
    }

    public function TransactionPulsa()
    {
        return $this->hasMany('App\Models\TransactionPulsa', 'denom_id');
    }

    public function TransactionPacketData()
    {
        return $this->hasMany('App\Models\TransactionPacketData', 'denom_id');
    }

    public function TransactionPlnPrepaid()
    {
        return $this->hasMany('App\Models\TransactionPlnPrepaid', 'denom_id');
    }

    public function TransactionPlnPostpaid()
    {
        return $this->hasMany('App\Models\TransactionPlnPostpaid', 'denom_id');
    }

    public function transactionGame()
    {
        return $this->hasMany('App\Models\TransactionGame', 'denom_id');
    }
}
