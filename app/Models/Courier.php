<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    protected $table = 'couriers';

    protected $fillable = [
        'name',
        'code',
        'logo'
    ];

    protected $hidden = [
        'created_at','updated_at'
    ];

    public function transaction()
    {
        return $this->hasMany('App\Models\Transaction', 'courier_id');
    }
}
