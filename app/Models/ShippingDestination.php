<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingDestination extends Model
{
    protected $table='shipping_destinations';
    protected $fillable = [
        'user_id',
        'address_name',
        'recipient_name',
        'destination_detail'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

}
