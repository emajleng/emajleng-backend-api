<?php

namespace App\Http\Controllers;

use App\Events\PascaTransactionEvent;
use App\Events\TopUpTransactionEvent;
use App\Helper\Client;
use App\Helper\ResponseHelper;
use App\Models\Denom;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;

class PpobTransactionController extends Controller
{
    public function checkUser($amount){
        if(auth()->user()->balance>=$amount){
            return true;
        }
        return false;
    }

    public function checkAmount($code, $admin_fee){
        $amount = 0;
        $urlData=[
            "commands"=> 'prepaid',
            'username'=>env('UsernameDigiflazz'),
            'sign'=>md5(env('UsernameDigiflazz').env('KeyDigiflazz').'pricelist')
        ];
        $digiflazz = Client::digiflazz($urlData,'price-list');
        foreach($digiflazz->data as $digi){
            if($code == $digi->buyer_sku_code){
                $amount = $digi->price + $admin_fee;
            }
        }
        return $amount;
    }

    public function checkAmountPasca($digiflazz, $admin_fee){
        $amount=0;
        $benefits = ($digiflazz->selling_price-$digiflazz->price)/$digiflazz->desc->lembar_tagihan;
        foreach($digiflazz->desc->detail as $digi){
           $amount += $digi->nilai_tagihan+$digi->denda+$digi->admin;
        }
        $amount = ($amount + $admin_fee)-$benefits;
        return $amount;
    }

    public function inquiryPasca($customer_no, $ref_id){
        $commands='inq-pasca';
        $data=[
            "commands"=> $commands,
            'username'=>env('UsernameDigiflazz'),
            'buyer_sku_code'=>'PLNPOSTPAID',
            'customer_no'=>$customer_no,
            'ref_id'=>$ref_id,
            'sign'=>md5(env('UsernameDigiflazz').env('KeyDigiflazz').$ref_id)
        ];
        $digiflazz = Client::digiflazz($data,'transaction');
        return $digiflazz->data;
    }

    public function paymentPasca($customer_no, $ref_id, $amount, $denom_id, $inquiry,$admin_fee,$code){
        $commands='pay-pasca';
        $data=[
            "commands"=> $commands,
            'username'=>env('UsernameDigiflazz'),
            'buyer_sku_code'=>$code,
            'customer_no'=>$customer_no,
            'ref_id'=>$ref_id,
            'sign'=>md5(env('UsernameDigiflazz').env('KeyDigiflazz').$ref_id)
        ];
        $digiflazz = Client::digiflazz($data,'transaction');
        if($digiflazz->data->rc=='00'){
            $this->minusBalance($amount);
            event(new PascaTransactionEvent(
                $denom_id,
                auth()->user()->merchant->id,
                $amount,
                $customer_no,
                $digiflazz->data->customer_name,
                $digiflazz->data->status,
                json_encode($digiflazz->data),
                'PLN_POSTPAID',
                $digiflazz->data->desc->tarif,
                json_encode($digiflazz->data->desc->detail),
                $digiflazz->data->desc->daya,
                $ref_id,
                auth()->user()->merchant->balance,
                $admin_fee
            ));
        }else if($digiflazz->data->rc=='03'||$digiflazz->data->rc=='99'){
            $this->minusBalance($amount);
            event(new PascaTransactionEvent(
                $denom_id,
                auth()->user()->merchant->id,
                $amount,
                $customer_no,
                $inquiry->customer_name,
                $digiflazz->data->rc=='99'?'Gangguan':$digiflazz->data->status,
                json_encode($digiflazz->data),
                'PLN_POSTPAID',
                $inquiry->tarif,
                json_encode($inquiry->detail),
                $inquiry->voltage,
                $ref_id,
                auth()->user()->merchant->balance,
                $admin_fee
            ));
        }
        return $digiflazz->data;
    }

    public function minusBalance($amount){
        $query = User::query()->where('id',auth()->user()->id);
        $merchant = $query->sharedLock()->first();
        $merchant->balance -= $amount;
        $merchant->save();
        return true;
    }

    public function plusBalance($amount){
        $query = User::query()->where('id',auth()->user()->id);
        $merchant = $query->sharedLock()->first();
        $merchant->balance += $amount;
        $merchant->save();
        return true;
    }

    public function checkRefId($ref_id,$type){
        if($type=='PULSA'){
            $query = auth()->user()->transactionPulsa();
        }elseif($type=='DATA'){
            $query = auth()->user()->transactionPacketData();
        }elseif($type=='EMONEY'){
            $query = auth()->user()->transactionEmoney();
        }elseif($type=='PLN'){
            $query = auth()->user()->transactionPlnPrepaid();
        }elseif($type=='PLN_POSTPAID'){
            $query = auth()->user()->transactionPlnPostpaid();
        }
        $data = $query->where('ref_id',$ref_id)->first();
        return $data;
    }

    public function topup(Request $request){
        $validationType = array('PULSA','EMONEY','DATA','PLN');
        $validator = Validator::make($request->all(), [
            'denom_id' => 'required|numeric|exists:denom,id',
            'customer_no' => 'required|numeric',
            'type' => 'required|in:' . implode(',', $validationType),
            'customer_name' => $request->type=='PLN'? 'required|string':'string',
            'tarif' => $request->type=='PLN'? 'required|string':'string',
            'voltage' => $request->type=='PLN'? 'required|string':'string',
        ]);

        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }

        DB::beginTransaction();
        try{
            $denom = Denom::query()->where('id',$request->denom_id)->first();
            $amount = $this->checkAmount($denom->code, $denom->admin_fee);
            $this->minusBalance($amount);
        }catch(Exception $e){
            DB::rollBack();
            return ResponseHelper::badRequest(['Saldo anda tidak mencukupi'], "Validation required");
        }
        DB::commit();

        $ref_id = Carbon::now()->unix().auth()->user()->id.'-'.$request->type;
        $customerNumber = $request->customer_no;

        $data=[
            'username'=>env('UsernameDigiflazz'),
            'buyer_sku_code'=>$denom->code,
            'customer_no'=>$customerNumber,
            'ref_id'=>$ref_id,
            'sign'=>md5(env('UsernameDigiflazz').env('KeyDigiflazz').$ref_id),
            'msg'=>$request->type,
//            'testing'=>true
        ];
        $digiflazz = Client::digiflazz($data,'transaction');
        if($digiflazz->data->rc=='00'||$digiflazz->data->rc=='03'||$digiflazz->data->rc=='99'){
            if($request->type=='PLN'){
                event(new PascaTransactionEvent(
                    $request->denom_id,
                    auth()->user()->id,
                    $amount,
                    $request->customer_no,
                    $request->customer_name,
                    $digiflazz->data->rc=='99'?'Gangguan':$digiflazz->data->status,
                    json_encode($digiflazz->data),
                    $request->type,
                    $request->tarif,
                    $digiflazz->data->sn,
                    $request->voltage,
                    $ref_id,
                    auth()->user()->balance,
                    $denom->admin_fee
                ));
                if($digiflazz->data->rc=='03'||$digiflazz->data->rc=='99'){
                    $digiflazz->data->sn="Silahkan Tunggu Beberapa saat";
                }
            }else{
                event(new TopUpTransactionEvent(
                    $request->denom_id,
                    auth()->user()->id,
                    $amount,
                    $request->customer_no,
                    $digiflazz->data->rc=='99'?'Gangguan':$digiflazz->data->status,
                    json_encode($digiflazz->data),
                    $request->type,
                    $ref_id,
                    auth()->user()->balance,
                    $denom->admin_fee
                ));
            }
            return ResponseHelper::ok($digiflazz->data);
        }else{
            $this->plusBalance($amount);
            return ResponseHelper::serviceUnavailable($digiflazz->data);
        }
    }

    public function pasca(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_no' => 'required|numeric',
            'type' => 'required',
//            'denom_id' =>'required|numeric|exists:denom,id',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }

        $ref_id=  strval(mt_rand(00000000, 99999999));
        while(strlen($ref_id)!=8||$this->checkRefId($ref_id,'PLN_POSTPAID')) {
            if(strlen($ref_id)!=8||$this->checkRefId($ref_id,'PLN_POSTPAID')){
                $ref_id=  strval(mt_rand(00000000, 99999999));
            }else{
                break;
            }
        }
        $ref_id= $ref_id.'-PLN_POSTPAID';
        if($request->type=='PAYMENT'){
            $denom = Denom::query()->where('id',$request->denom_id)->first();
            $inquiry = $this->inquiryPasca($request->customer_no, $ref_id);

            $amount = $this->checkAmountPasca($inquiry, $denom->admin_fee);
            if(!$this->checkUser($amount)){
                return ResponseHelper::badRequest(['Saldo anda tidak mencukupi'], "Validation required");
            }
            $digiflazz = $this->paymentPasca($request->customer_no, $ref_id, $amount, $request->denom_id, $inquiry, $denom->admin_fee, $denom->code);
        }else{
            $digiflazz = $this->inquiryPasca($request->customer_no, $ref_id);
        }
        if($digiflazz->rc=='00'||$digiflazz->rc=='03'||$digiflazz->rc=='99'){
            return ResponseHelper::ok($digiflazz);
        }elseif($digiflazz->rc=='46'||$digiflazz->rc=='47'||$digiflazz->rc=='50'||$digiflazz->rc=='51'||$digiflazz->rc=='54'||$digiflazz->rc=='60'||$digiflazz->rc=='58'||$digiflazz->rc=='59'||$digiflazz->rc=='66') {
            return ResponseHelper::badRequest([$digiflazz->message],'Validation Required');
        }else{
            return ResponseHelper::serviceUnavailable($digiflazz);
        }
    }

    public function inquiry(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_no' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $data=[
            'commands'=>'pln-subscribe',
            'customer_no'=>$request->customer_no,
        ];
        $digiflazz = Client::digiflazz($data,'transaction');
        return ResponseHelper::ok($digiflazz->data);
    }

    public function getTransaction(Request $request){
        $validationType = array('PULSA','EMONEY','DATA','PLN','PLN_POSTPAID');
        $validator = Validator::make($request->all(), [
            'type' => 'required|in:' . implode(',', $validationType),
            'page' => 'required|integer|min:0'
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        if($request->type=='PULSA'){
            $data = auth()->user()->transactionPulsa();
        }elseif($request->type=='DATA'){
            $data = auth()->user()->transactionPacketData();
        }elseif($request->type=='EMONEY'){
            $data = auth()->user()->transactionEmoney();
        }elseif($request->type=='PLN'){
            $data = auth()->user()->transactionPlnPrepaid();
        }elseif($request->type=='PLN_POSTPAID'){
            $data = auth()->user()->transactionPlnPostpaid();
        }
        $totalPage = ceil($data
            ->get()
            ->count() / 10);
        $transactions = $data->orderBy('updated_at', 'desc')
            ->skip($request['page']*10)
            ->take(10)
            ->get();
        foreach($transactions as $transaction){
            $denom = Denom::query()->find($transaction->denom_id);
            $transaction['denom_id'] = $denom;
            $transaction['denom_id']['provider'] = $denom->provider;
            $transaction['denom_id']['provider']->logo = env('AdminUrl').$denom->provider->logo;
            if($request->type=='PLN_POSTPAID'){
                $transaction['period'] = json_decode($transaction->period);
            }
        }
        return ResponseHelper::paging($transactions, $request['page'], $totalPage);
    }
}
