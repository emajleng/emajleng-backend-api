<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserBankController extends Controller
{
    public function getUserBank(){
        $data =  auth()->user()->UserBank()->where('status',true)->get();
        return ResponseHelper::ok($data);
    }

    public function createUserBank(Request $request){
        $validator = Validator::make($request->all(), [
            'bank_name' => 'required',
            'account_name' => 'required',
            'account_number' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        auth()->user()->userBank()->create([
            'bank_name'=>$request->bank_name,
            'account_number'=>$request->account_number,
            'account_name'=>$request->account_name,
            'status'=>true
        ]);
        return ResponseHelper::ok(true);
    }

    public function updateUserBank(Request $request,$id){
        $validator = Validator::make($request->all(), [
            'bank_name' => 'required',
            'account_name' => 'required',
            'account_number' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        auth()->user()->userBank()->where('id',$id)->first()->update([
            'bank_name'=>$request->bank_name,
            'account_number'=>$request->account_number,
            'account_name'=>$request->account_name
        ]);
        return ResponseHelper::ok(true);
    }

    public function deleteUserBank($id){
        auth()->user()->userBank()->where('id',$id)->first()->update([
            'status'=>false
        ]);
        return ResponseHelper::ok(true);
    }
}
