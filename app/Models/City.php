<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'id',
        'province_id',
        'name',
    ];

    public function districts()
    {
        return $this->hasMany('App\Models\Districts', 'city_id');
    }

    public function merchant()
    {
        return $this->hasMany('App\Models\Merchant', 'city_id');
    }

    public function user()
    {
        return $this->hasMany('App\Models\User', 'city_id');
    }

    public function province(){
        return $this->belongsTo('App\Models\Province','province_id','id');
    }
}
