<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminFee extends Model
{
    protected $table = 'admin_fees';

    protected $hidden = [
        'id','created_at','updated_at'
    ];
}
