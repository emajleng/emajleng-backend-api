<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    public function getCart(){
        $carts = auth()->user()->cart()->get();
        $data=[];
        foreach ($carts as $cart){
            $response=[
                'id'=>$cart->id,
                'product'=>[
                    'id' => $cart->product->id,
                    'merchant' => $cart->product->merchant,
                    'phone' => $cart->product->merchant->user->phone,
                    'productCategory' => $cart->product->productCategory,
                    'name' => $cart->product->name,
                    'weight' => $cart->product->weight,
                    'price' => $cart->product->price,
                    'stock' => $cart->product->stock,
                    'description' => $cart->product->description,
                    'photo' => url($cart->product->photo),
                ],
                'quantity'=>$cart->quantity
            ];
            array_push($data,$response);
        }
        return ResponseHelper::ok($data);
    }

    public function addProductToCart(Request $request){
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'quantity' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $carts = auth()->user()->cart()->get();
        $isUpdated = false;
        foreach ($carts as $cart){
            if($cart->product_id==$request->product_id){
                auth()->user()->cart()->where('product_id',$request->product_id)->first()->update([
                   'quantity' =>  $request->quantity+$cart->quantity
                ]);
                $isUpdated=true;
            }
        }
        if(!$isUpdated){
            auth()->user()->cart()->create([
                'product_id'=>$request->product_id,
                'quantity'=>$request->quantity,
            ]);
        }
        return ResponseHelper::ok(true);
    }

    public function updateProductCart(Request $request,$id){
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'quantity' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        auth()->user()->cart()->where('id',$id)->first()->update([
            'quantity'=>$request->quantity,
        ]);
        return ResponseHelper::ok(true);
    }

    public function deleteCart($id){
        $cart = Cart::query()->find($id);
        $cart->delete();
        return ResponseHelper::ok(true);
    }
}
