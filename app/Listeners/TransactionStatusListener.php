<?php

namespace App\Listeners;

use App\Events\TransactionStatusEvent;
use App\Models\User;
use App\Notifications\FirebaseTransactionStatusNotification;
use Illuminate\Contracts\Queue\ShouldQueue;

class TransactionStatusListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\TransactionStatusEvent  $event
     * @return void
     */
    public function handle(TransactionStatusEvent $event)
    {
        $data = $event->data['data'];
        $user = User::query()->find($event->data['user_id']);
        $user->notify(new FirebaseTransactionStatusNotification($data));
//        $user->notify(new EmailTransactionStatusNotification($data));
    }
}
