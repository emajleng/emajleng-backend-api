<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = 'provider';

    protected $fillable = [
        'name'
    ];

    public function Denom()
    {
        return $this->hasMany('App\Models\Denom', 'provider_id');
        // ->orderByRaw('CHAR_LENGTH(amount)')->orderBy('amount');
    }
}
