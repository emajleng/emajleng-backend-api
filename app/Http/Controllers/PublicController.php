<?php

namespace App\Http\Controllers;

use App\Helper\Client;
use App\Helper\ResponseHelper;
use App\Models\AdminFee;
use App\Models\Banner;
use App\Models\City;
use App\Models\Courier;
use App\Models\Districts;
use App\Models\Menu;
use App\Models\News;
use App\Models\PaymentMethod;
use App\Models\ProductCategory;
use App\Models\Provider;
use App\Models\Province;
use App\Models\Village;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function getAdminFee(){
        $data = AdminFee::query()->first();
        return ResponseHelper::ok($data);
    }

    public function getMenu(){
        $Menus = Menu::query()->get();
        $data = [];
        foreach($Menus as $menu){
            $response=[
                'id' => $menu->id,
                'icon' => env('AdminUrl').$menu->icon,
                'name' => $menu->name,
                'status' => $menu->status,
                'route' => $menu->route,
                'type' => $menu->type,
            ];
            array_push($data, $response);
        }
        return ResponseHelper::ok($data);
    }

    public function getCourier(){
        $couriers = Courier::query()->get();
        $data = [];
        foreach($couriers as $courier){
            $response=[
                'id' => $courier->id,
                'icon' => env('AdminUrl').$courier->logo,
                'name' => $courier->name,
                'code' => $courier->code,
            ];
            array_push($data, $response);
        }
        return ResponseHelper::ok($data);
    }

    public function getProvince(){
        $data = Province::query()->get();
        return ResponseHelper::ok($data);
    }

    public function getPaymentMethod(){
        $data = PaymentMethod::query()->get();
        return ResponseHelper::ok($data);
    }

    public function getProductCategory(){
        $data = ProductCategory::query()->get();
        return ResponseHelper::ok($data);
    }

    public function getBanner(){
        $datas = Banner::query()->get();
        foreach ($datas as $data){
            $data['image']=env('UriAdmin').$data['image'];
        }
        return ResponseHelper::ok($datas);
    }

    public function getNews(){
        $news = News::query()->orderBy('created_at','DESC')->get();
        $data=[];
        foreach ($news as $new){
            $response=[
                'id' => $new->id,
                'title' => $new->title,
                'content' => $new->content,
                'image' => env('AdminUrl').$new->image,
                'created_at' => $new->created_at,
                'updated_at' => $new->updated_at,
            ];
            array_push($data,$response);
        }
        return ResponseHelper::ok($data);
    }

    public function getNewsById($id){
        $news = News::query()->where('id',$id)->first();
        $response=[
            'id' => $news->id,
            'title' => $news->title,
            'content' => $news->content,
            'image' => env('AdminUrl').$news->image,
            'created_at' => $news->created_at,
            'updated_at' => $news->updated_at,
        ];
        return ResponseHelper::ok($response);
    }

    public function getCity($id){
        $data = City::query()->where('province_id',$id)->get();
        return ResponseHelper::ok($data);
    }

    public function getDistrict($id){
        $data = Districts::query()->where('city_id',$id)->get();
        return ResponseHelper::ok($data);
    }

    public function getVillage($id){
        $data = Village::query()->where('district_id',$id)->get();
        return ResponseHelper::ok($data);
    }

    public function getProvider(Request $request){
        $data = [];
        $urlData=[
            "commands"=> 'prepaid',
            'username'=>env('UsernameDigiflazz'),
            'sign'=>md5(env('UsernameDigiflazz').env('KeyDigiflazz').'pricelist')
        ];
        $digiflazz = Client::digiflazz($urlData,'price-list');
        $providers = Provider::query();
        if($request->name){
            $providers = $providers->where('name','LIKE','%'.$request->name.'%')->first();
            $data = $this->getDenom($providers,$digiflazz);
            return ResponseHelper::ok($data);
        }else{
            $providers = $providers->get();
            foreach($providers as $provider){
                $denoms = $this->getDenom($provider,$digiflazz);
                array_push($data, $denoms);
            }
            return ResponseHelper::ok($data);
        }
    }

    public function getDenom($provider, $digiflazz){
        $denoms=[];
        $price=null;
        $multi=null;
        $stock=null;
        $unlimited_stock=null;
        $seller_product_status=null;
        foreach($provider->denom as $denom){
            foreach($digiflazz->data as $digi){
                if($denom->code==$digi->buyer_sku_code){
                    $price=$digi->price;
                    $stock=$digi->stock;
                    $multi=$digi->multi;
                    $unlimited_stock=$digi->unlimited_stock;
                    $seller_product_status=$digi->seller_product_status;
                }
            }
            $responseDenom=[
                'id'=>$denom->id,
                'amount'=>$denom->amount,
                'code'=>$denom->code,
                'admin_fee'=>$denom->admin_fee,
                'status'=>$denom->status,
                'price'=>$price,
                'stock'=>$stock,
                'multi'=>$multi,
                'unlimited_stock'=>$unlimited_stock,
                'seller_product_status'=>$seller_product_status
            ];
            array_push($denoms, $responseDenom);
        }
        $denom = collect($denoms);
        $sortDenom = $denom->sortBy('price');
        return [
            'id' => $provider->id,
            'name' => $provider->name,
            'logo' => env('AdminUrl').$provider->logo,
            'denom' => $sortDenom->values()->all()
        ];
    }
}
