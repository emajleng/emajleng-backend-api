<?php

namespace App\Listeners;

use App\Events\NotificationEvent;
use App\Models\User;
use App\Notifications\SuccessTransaction;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\NotificationEvent  $event
     * @return void
     */
    public function handle(NotificationEvent $event)
    {
        $data = $event->data['data'];
        $user = User::query()->find($event->data['user_id']->user_id);
        $user->notify(new SuccessTransaction($data));
    }
}
