<?php

namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use App\Events\TransactionStatusEvent;
use App\Helper\ResponseHelper;
use App\Models\Merchant;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    public function createTransaction(Request $request){
        $validator = Validator::make($request->all(), [
            'payment_method_id' => 'required|numeric|exists:payment_methods,id',
            'amount' => 'required|numeric',
            'detail' => 'required|array',
            'detail.*.product_id' => 'required|numeric|exists:products,id',
            'detail.*.quantity' => 'required|integer',
            'logistic.courier_id' => 'required|numeric|exists:couriers,id',
            'logistic.courier_service' => 'required|string',
            'logistic.postal_fee' => 'required|numeric',
            'logistic.etd' => 'required|string',
            'origin_details' => 'required',
            'destination_details' => 'required',
            'is_balance' => 'required|boolean'
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }

        $productId = $request->detail[0]['product_id'];
        $merchantId = Product::query()->find($productId)->merchant_id;
        $merchant = Merchant::query()->find($merchantId);
        if(!$merchant->status){
            return ResponseHelper::badRequest(["Merchant is inactive"], "Invalid Id");
        }

        $increment=0;
        foreach ($request->detail as $detail){
            if($increment>0){
                $requestProductId = $detail['product_id'];
                $requestMerchantId = Product::query()->find($requestProductId)->merchant_id;
                if ($merchantId != $requestMerchantId) {
                    return ResponseHelper::badRequest(['Transaksi hanya bisa dari satu toko'], 'Validation Error');
                }
            }
            $productId = $detail['product_id'];
            $merchantId = Product::query()->find($productId)->merchant_id;
            $increment += 1;
        }

        $status = 'WAITING_PAYMENT';
        if($request->is_balance){
            if(!$this->checkUser($request->amount)){
                return ResponseHelper::badRequest(['Saldo anda tidak mencukupi'], "Validation required");
            }
            $this->minusBalance($request->amount);
            $status = 'REQUESTED';
        }
        $data=auth()->user()->transaction()->create([
            'merchant_id' => $merchantId,
            'payment_method_id' => $request->payment_method_id,
            'courier_id' => $request->logistic['courier_id'],
            'courier_service' => $request->logistic['courier_service'],
            'amount' => $request->amount,
            'unique_code' => mt_rand(001, 999),
            'detail' => json_encode($request->detail),
            'status' => $status,
            'postal_fee' => $request->logistic['postal_fee'],
            'delivery_time' => $request->logistic['etd'],
            'origin_details' => json_encode($request->origin_details),
            'destination_details' => json_encode($request->destination_details)
        ]);
        return ResponseHelper::ok($data);
    }

    public function getUserTransaction(Request $request){
        $validationType = array('WAITING_PAYMENT','REQUESTED','PROCESSED','ACCEPTED','REJECTED','DELIVERING','FINISHED');
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:' . implode(',', $validationType),
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $query = auth()->user()->transaction()->where('status',$request->status);

        $totalPage = ceil($query
                ->get()
                ->count() / 10);

        $transactions = $query->orderBy('updated_at', 'desc')
            ->skip($request['page']*10)
            ->take(10)
            ->get();

        $data=[];
        foreach ($transactions as $transaction){
            $dataProduct=[];
            $transactionProducts = json_decode($transaction->detail);

            foreach ($transactionProducts as $transactionProduct){
                $product=Product::query()->find($transactionProduct->product_id);
                $responseProduct = [
                    'product'=>[
                        'id' => $product->id,
                        'productCategory' => $product->productCategory,
                        'name' => $product->name,
                        'price' => $product->price,
                        'stock' => $product->stock,
                        'weight' => $product->weight,
                        'description' => $product->description,
                        'photo' => url($product->photo),
                    ],
                    'quantity'=>$transactionProduct->quantity
                ];
                array_push($dataProduct,$responseProduct);
            }

            $response=[
                'id'=>$transaction->id,
                'merchant' => [
                    'id' => $transaction->merchant->id,
                    'phone' => $transaction->merchant->user->phone,
                    'name' => $transaction->merchant->name,
                    'logo' => url($transaction->merchant->logo),
                    'address' => $transaction->merchant->address,
                    'province' => $transaction->merchant->province,
                    'city' => $transaction->merchant->city,
                    'district' => $transaction->merchant->districts,
                    'village' => $transaction->merchant->village
                ],
                'payment_method' => $transaction->payment_method,
                'courier' => $transaction->courier->name,
                'courier_service' => $transaction->courier_service,
                'amount' => $transaction->amount,
                'unique_code' => $transaction->unique_code,
                'detail' => $dataProduct,
                'status' => $transaction->status,
                'postal_fee' => $transaction->postal_fee,
                'delivery_time' => $transaction->etd,
                'origin_details' => json_decode($transaction->origin_details),
                'destination_details' => json_decode($transaction->destination_details),
                'airway_bill' => $transaction->airway_bill,
                'created_at' => $transaction->created_at,
                'updated_at' => $transaction->updated_at,
            ];
            array_push($data,$response);
        }

        return ResponseHelper::paging($data, $request['page'], $totalPage);
    }

    public function getMerchantTransaction(Request $request){
        $validationType = array('WAITING_PAYMENT','REQUESTED','PROCESSED','ACCEPTED','REJECTED','DELIVERING','FINISHED');
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:' . implode(',', $validationType),
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $query = auth()->user()->merchant->transaction()->where('status',$request->status);

        $totalPage = ceil($query
                ->get()
                ->count() / 10);

        $transactions = $query->orderBy('updated_at', 'desc')
            ->skip($request['page']*10)
            ->take(10)
            ->get();

        $data=[];
        foreach ($transactions as $transaction){
            $dataProduct=[];
            $transactionProducts = json_decode($transaction->detail);

            foreach ($transactionProducts as $transactionProduct){
                $product=Product::query()->find($transactionProduct->product_id);
                $responseProduct = [
                    'product'=>[
                        'id' => $product->id,
                        'productCategory' => $product->productCategory,
                        'name' => $product->name,
                        'price' => $product->price,
                        'stock' => $product->stock,
                        'weight' => $product->weight,
                        'description' => $product->description,
                        'photo' => url($product->photo),
                    ],
                    'quantity'=>$transactionProduct->quantity
                ];
                array_push($dataProduct,$responseProduct);
            }

            $response=[
                'id'=>$transaction->id,
                'user' => [
                    'id' => $transaction->user->id,
                    'name' => $transaction->user->name,
                    'phone' => $transaction->user->phone,
                    'logo' => url($transaction->user->logo),
                    'address' => $transaction->user->address,
                    'province' => $transaction->user->province,
                    'city' => $transaction->user->city,
                    'district' => $transaction->user->districts,
                    'village' => $transaction->user->village
                ],
                'payment_method' => $transaction->payment_method,
                'courier' => $transaction->courier->name,
                'courier_service' => $transaction->courier_service,
                'amount' => $transaction->amount,
                'unique_code' => $transaction->unique_code,
                'detail' => $dataProduct,
                'status' => $transaction->status,
                'postal_fee' => $transaction->postal_fee,
                'delivery_time' => $transaction->etd,
                'origin_details' => json_decode($transaction->origin_details),
                'destination_details' => json_decode($transaction->destination_details),
                'airway_bill' => $transaction->airway_bill,
                'created_at' => $transaction->created_at,
                'updated_at' => $transaction->updated_at,
            ];
            array_push($data,$response);
        }

        return ResponseHelper::paging($data, $request['page'], $totalPage);
    }

    public function confirmationPayment(Request $request,$id){
        $validator = Validator::make($request->all(), [
            'photo' => 'required|image',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $transaction = auth()->user()->transaction()->where('id',$id)->first();
        if(!$transaction){
            return ResponseHelper::badRequest(['Transaksi tidak valid'],'Validation error');
        }
        if($transaction->status!='WAITING_PAYMENT'){
            return ResponseHelper::badRequest(['Status tidak valid'],'Validation error');
        }

        $file = $request->photo;
        $image_path = 'storage/transaction/confirmation/';
        $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
        $file->move($image_path, $filename);
        $transaction->payment_confirmation = $filename;
        $transaction->save();

        return ResponseHelper::ok(true);
    }

    public function userCancelTransaction($id){
        $transaction = auth()->user()->transaction()->where('id',$id)->first();
        if(!$transaction){
            return ResponseHelper::badRequest(['Transaksi tidak valid'],'Validation error');
        }
        if($transaction->status!='WAITING_PAYMENT'){
            return ResponseHelper::badRequest(['Status tidak valid'],'Validation error');
        }
        $transaction->delete();
        return ResponseHelper::ok(true);
    }

    public function processTransaction($id){
        $transaction = auth()->user()->merchant->transaction()->where('id',$id)->first();
        if(!$transaction){
            return ResponseHelper::badRequest(['Transaksi tidak valid'],'Validation error');
        }
        if($transaction->status!='REQUESTED'){
            return ResponseHelper::badRequest(['Status tidak valid'],'Validation error');
        }

        $transactionDetail = json_decode($transaction->detail);
        foreach ($transactionDetail as $transactionProduct){
           $product =  Product::query()->find($transactionProduct->product_id);
           $product->stock -= $transactionProduct->quantity;
           $product->save();
        }

        $transaction->status = 'PROCESSED';
        $transaction->save();

        $data = [
            'title' => 'Transaksi Anda Sedang Diproses',
            'notification' => 'Transaksi anda telah diproses oleh penjual dan akan segera dikirim',
        ];
        event(new TransactionStatusEvent($data, $transaction->user_id));
        return ResponseHelper::ok(true);
    }

    public function rejectTransaction($id){
        $transaction = auth()->user()->merchant->transaction()->where('id',$id)->first();
        if(!$transaction){
            return ResponseHelper::badRequest(['Transaksi tidak valid'],'Validation error');
        }
        if($transaction->status!='REQUESTED'){
            return ResponseHelper::badRequest(['Status tidak valid'],'Validation error');
        }
        $transaction->status = 'REJECTED';

        $user = User::query()->find($transaction->user_id);
        $user->balance += $transaction->amount;

        $transaction->save();
        $user->save();

        $data = [
            'title' => 'Transaksi Anda Sedang Ditolak',
            'notification' => 'Transaksi anda ditolak oleh penjual dan saldo anda kan segera dikembalikan',
        ];
        event(new TransactionStatusEvent($data, $transaction->user_id));
        return ResponseHelper::ok(true);
    }

    public function deliverProduct(Request $request,$id){
        $validator = Validator::make($request->all(), [
            'airway_bill' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $transaction = auth()->user()->merchant->transaction()->where('id',$id)->first();
        if(!$transaction){
            return ResponseHelper::badRequest(['Transaksi tidak valid'],'Validation error');
        }
        if($transaction->status!='PROCESSED'){
            return ResponseHelper::badRequest(['Status tidak valid'],'Validation error');
        }
        $transaction->status = 'DELIVERING';
        $transaction->airway_bill = $request->airway_bill;
        $transaction->save();

        $data = [
            'title' => 'Transaksi Anda Sudah Dikirim',
            'notification' => 'Transaksi anda sudah dikirim oleh penjual dan dengan no resi '.$request->airway_bill,
        ];
        event(new TransactionStatusEvent($data, $transaction->user_id));
        return ResponseHelper::ok(true);
    }

    public function finishTransaction($id){
        $transaction = auth()->user()->transaction()->where('id',$id)->first();
        if(!$transaction){
            return ResponseHelper::badRequest(['Transaksi tidak valid'],'Validation error');
        }
        if($transaction->status!='DELIVERING'){
            return ResponseHelper::badRequest(['Status tidak valid'],'Validation error');
        }
        $transaction->status = 'FINISHED';

        $userMerchant = User::query()->find($transaction->merchant->user_id);
        $userMerchant->balance += $transaction->amount;

        $transaction->save();
        $userMerchant->save();
        return ResponseHelper::ok(true);
    }

    public function minusBalance($amount){
        $user= User::query()->find(auth()->user()->id);
        $user->balance -= $amount;
        $user->save();
        return true;
    }

    public function checkUser($amount){
        if(auth()->user()->balance>=$amount){
            return true;
        }
        return false;
    }
}
