<?php

namespace App\Http\Controllers;

use App\Events\TransactionStatusEvent;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function testNotification(){
        event(new TransactionStatusEvent("TEST","2"));
    }
}
